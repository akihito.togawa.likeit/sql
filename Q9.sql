SELECT
	t1.category_name,
	sum(t.item_price) AS total_price

FROM
	item t

INNER JOIN
	item_category t1
ON
	t.category_id =t1.category_id


GROUP BY
	t1.category_id

ORDER BY
	 total_price DESC;
